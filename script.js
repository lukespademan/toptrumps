var DEBUG = true;
var gameOver = false;
var gameStarted = false;
var sleepTime = 2000;

var all_card_props = [];
class Player {
    constructor(name, cards) {
        this.name = name;
        this.active = false;
        if (cards != null) {
            this.cards = cards;
        }
        else {
            this.cards = [];
        }
    }
    draw_area() {
        this.area_id = this.name.replace(' ', '');
        this.card_area_id = this.area_id + 'cards';
        let div = document.createElement('div');
        div.id = this.area_id;
        let heading = document.createElement(this.active ? 'h1': 'h2');
        heading.innerHTML = this.name;
        let card_area = document.createElement('div');
        card_area.id = this.card_area_id;
        div.appendChild(heading);
        div.appendChild(card_area);
        draw(div);
    }
    add_card(card) {
        this.cards.push(card);
    }
    add_cards(cards) {
        for (let i = 0; i < cards.length; i++) {
            this.add_card(cards[i]);
        }
    }
}
class Card {
    constructor(name, props) {
        this.name = name;
        this.props = props; // props is an object. example {speed: 5 weight: 4}
        this.id;
    }
    get_value(prop) {
        if (hasProp(this.props, prop)) {
            return this.props[prop];
        }
        return false;
    }
    create_element(active=false) {
        // visibility:hidden
        let div = document.createElement('div');
        div.className = 'card'
        div.className += active ? '' : ' deactive';
        if (this.id) {
            div.id = this.id;
        }
        if (gameStarted === false) {
            let editBtn = document.createElement('button');
            editBtn.onclick = () => editCard(this.id-1);
            editBtn.innerHTML = 'edit';
            div.appendChild(editBtn);
        }

        let name_heading = document.createElement('h3');
        name_heading.innerHTML = this.name;
        div.appendChild(name_heading);


        let attributes_table = document.createElement('table');
        for (let prop in this.props) {
            if (this.props.hasOwnProperty(prop)) {
                let row = document.createElement('tr');
                let prop_data = document.createElement('td');
                prop_data.innerHTML = prop;
                let value_data = document.createElement('td');
                value_data.innerHTML = this.props[prop];
                row.appendChild(prop_data);
                row.appendChild(value_data);
                attributes_table.appendChild(row);
            }
        }
        
        
        
        div.appendChild(attributes_table);
        return div;
    }
}

function shuffle(array) {  // from https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
    var currentIndex = array.length, temporaryValue, randomIndex;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
  
    return array;
  }

function hasProp(object, key) {
    return object ? hasOwnProperty.call(object, key) : false;
}
function linebreak() {
    return document.createElement('br');
}

function compare_cards(cd1, cd2, prop) {
    var cd1_val = cd1.get_value(prop);
    var cd2_val = cd2.get_value(prop);
    if (cd1_val === false || cd2_val === false) {
        return [false, 'not all cards selected have attribute ' + prop];
    }
    if (cd1_val === cd2_val) {
        return [false, 'both cards have the same value for ' + prop];
    }
    var winner_name = cd1_val > cd2_val ? cd1.name : cd2.name;
    var looser_name = cd1_val > cd2_val ? cd2.name : cd1.name;
    var winner_card = cd1_val > cd2_val ? cd1 : cd2;
    var looser_card = cd1_val > cd2_val ? cd2 : cd1;
    return [true, '', winner_name, looser_name, prop, winner_card, looser_card];
}
function editCard(id) {
    document.getElementById('editwrapper').style.display = 'none';
    document.getElementById('editView').style.display = 'inline';
    let editArea = document.getElementById('editView');
    editArea.classList.add('form');
    editArea.innerHTML = '';
    let card = all_cards[id];
    let i = 0
    let datalist = genDataList();
    let moreProps = document.createElement('button');
    moreProps.onclick = () => addRows(editArea);
    moreProps.innerHTML = 'add rows';
    editArea.appendChild(moreProps);

    let button = document.createElement('button');
    button.onclick = () => finishEdit(id);
    button.innerHTML = 'edit card';
    editArea.appendChild(button);

    let nameDiv = document.createElement('div');
    let nameEdit = document.createElement('input');
    nameEdit.value = card.name;
    nameEdit.classList.add('title');
    nameEdit.id = 'titleEdit';
    nameDiv.appendChild(nameEdit);
    editArea.appendChild(nameDiv);
    

    for (let prop in card.props) {
        i++;
        let label = document.createElement('label');
        label.id = prop;
        label.classList.add('prop');
        label.innerHTML = `<input list='props' value=${prop} />`;
        label.appendChild(datalist);
        let input = document.createElement('input');
        input.id = prop;
        input.name = prop;
        input.classList.add('value');
        input.value = card.props[prop];
        editArea.appendChild(label);
        editArea.appendChild(input);
        editArea.appendChild(linebreak());
    }

    
}
function finishEdit(id) {
    let editArea = document.getElementById('editView');
    let card = all_cards[id];
    let new_props = {};
    let nextProp;
    for (let i = 0; i < editArea.children.length; i++) {
        let node = editArea.children[i];
        if (node.tagName === "LABEL") {
            nextProp = node.children[0].value;
        }
        if (node.tagName === "INPUT") {
            if (nextProp != "") {
                new_props[nextProp] = parseInt(node.value);
            }
        }
    }
    card.props = new_props;
    card.name = document.getElementById('titleEdit').value;
    getAllCardProps();
    clear('editAll');
    for (let i = 0; i < all_cards.length; i++) {
        draw(all_cards[i].create_element(active=true), 'editAll');
    }
    //draw_all();
    document.getElementById('editwrapper').style.display = 'inline';
    document.getElementById('editView').style.display = 'none';
}
function compare_players(p1, p2, property) {
    switch (0) {
        case p1.cards.length:
            return [false, `game over. ${p2.name} wins`];
        case p2.cards.length:
            return [false, `game over. ${p1.name} wins`];
        default:
            let p1_card = p1.cards.shift(-1);
            let p2_card = p2.cards.shift(-1);
            let [success, error_str, winner_name, looser_name, prop, winner_card, looser_card] =
                compare_cards(p1_card, p2_card, property);
            if (success === false) {
                return [false, error_str];
            }
            else {
                if (winner_card == p1_card) {
                    return [true, p1.name, p1_card, p2_card];
                }
                else if (winner_card == p2_card) {
                    return [true, p2.name, p1_card, p2_card];
                }
            }
    }
}
function clear(place) {
    if (place === undefined) {
        place = 'root';
    }
    let root = document.getElementById(place);
    root.innerHTML = '';
}
function draw(element, parent) {
    if (parent === undefined) {
        parent = 'root';
    }
    let root = document.getElementById(parent);
    root.appendChild(element);
}


function draw_all(extra_card=[-1, -1]) {
    clear();

    for (let i = 0; i < all_players.length; i++) {
        let player = all_players[i];
        player.draw_area();
        clear(player.card_area_id);
        for (let j = 0; j < player.cards.length; j++) {
            let showCard;
            if (gameOver === true || gameStarted === false) {
                showCard = true;
            }
            else if (extra_card[0] === i && extra_card[1] === j) {
                showCard = true;
            }
            else {
                showCard = player.active ? j === 0 : false;
            }
            draw(player.cards[j].create_element(showCard), player.card_area_id);
        }
    }
}
function setDropdownProps(card1, card2) {
    let dd = document.getElementById("chooseProp");
    dd.innerHTML = '';
    let c1_props = Object.keys(card1.props);
    let c2_props = Object.keys(card2.props);
    console.table([c1_props, c2_props]);
    let options = [];
    for (let i = 0; i < c1_props.length; i++) {
        let prop = c1_props[i];
        if (c2_props.includes(prop) === true) {
            options.push(prop);
        }
    }
    console.table(options);
    for (let i = 0; i < options.length; i++) {
        let op = document.createElement('option');
        op.value = options[i];
        op.innerHTML = options[i];
        dd.appendChild(op);
    }
}
function message(value) {
    document.getElementById('msg').innerHTML = value;
}
function hideControls() {
    document.getElementById('controlarea').className  += ' hidden';
}
function btnpress() {
    draw_all([
        current_player + 1 < all_players.length ? current_player + 1 : 0,
        0
    ])

    setTimeout(function(){
        evaluateMove();
        nextMove();
    }, sleepTime);

}

function evaluateMove() {
    let prop = document.getElementById('chooseProp').value;
    
    let [success, value, cd1, cd2] =
        compare_players(all_players[0], all_players[1], prop);
    if (success === true) {
        if (value === all_players[0].name) {
            message(all_players[0].name + ' beat ' + all_players[1].name);
            all_players[0].add_cards([cd1, cd2]);
        }
        else {
            message(all_players[1].name + ' beat ' + all_players[0].name);
            all_players[1].add_cards([cd1, cd2]);
        }
    }
    else {
        message(value);
    }
    

    let p1 = all_players[0];
    let p2 = all_players[1];
    let winner;
    switch (0) {
        case p1.cards.length:  // p2 win
            winner = p2;
            break;
        case p2.cards.length:  // p1 win
            winner = p1;
            break;

        default:
            winner = false;
            break;
    }
    if (winner != false) {
        message(`${winner.name} won the game`);
        gameOver = true;
        draw_all();  // shows all cards
        hideControls();
    }
    setDropdownProps(all_players[0].cards[0], all_players[1].cards[0]);
}

function nextMove() {
    nextPlayer();
    draw_all();
    setDropdownProps(all_players[0].cards[0], all_players[1].cards[0]);
}


function nextPlayer() {
    all_players[current_player].active = false;
    current_player += 1;
    if (current_player >= all_players.length) {
        current_player = 0;
    }
    all_players[current_player].active = true;
}

function startGame() {
    // distrbutes cards
    all_cards = shuffle(all_cards);
    for (let i = 0; i < all_cards.length; i++) {
        all_players[i % all_players.length].add_card(all_cards[i])
    }

    current_player = all_players.length - 1;
    nextPlayer();
    gameStarted = true;
    setDropdownProps(all_players[0].cards[0], all_players[1].cards[0]); 
    document.getElementById('controlarea').classList.remove('deactive');
    document.getElementById('editwrapper').style.display = 'none';
    draw_all();
}
function getAllCardProps() {
    for (let i = 0; i < all_cards.length; i++) {
        for (let prop in all_cards[i].props) {
            if (all_card_props.includes(prop) === false) {
                all_card_props.push(prop);
            }
        }
    }
}

function genDataList() {
    let datalist = document.createElement('datalist');
    datalist.id = 'props';
    for (let i = 0; i < all_card_props.length; i++) {
        prop = all_card_props[i];
        let option = document.createElement('option');
        option.value = prop;
        datalist.appendChild(option);
    }
    return datalist;
}
function addRows(parent) {
    let datalist = genDataList();
    
    for (let i = 0; i < 3; i++) {
        let label = document.createElement('label');
        label.classList.add('prop');
        label.innerHTML = `<input list='props'/>`;
        label.appendChild(datalist);
        let input = document.createElement('input');
        input.classList.add('value');
        parent.appendChild(label);
        parent.appendChild(input);
        parent.appendChild(linebreak());
    }
}
function newCard() {
    document.getElementById('editwrapper').style.display = 'none';
    document.getElementById('newCardForm').style.display = 'inline';
    let datalist = genDataList();
    let editArea = document.getElementById('newCardForm');
    editArea.innerHTML = '';
    let moreProps = document.createElement('button');
    moreProps.onclick = () => addRows(editArea);
    moreProps.innerHTML = 'add rows';
    editArea.appendChild(moreProps);

    let addButton = document.createElement('button');
    addButton.onclick = () => createNewCard();
    addButton.innerHTML = 'add card';
    editArea.appendChild(addButton);
    let nameDiv = document.createElement('div');
    let nameEdit = document.createElement('input');
    nameEdit.classList.add('title');
    nameEdit.id = 'titleEdit';
    nameDiv.appendChild(nameEdit);
    editArea.appendChild(nameDiv);

    addRows(editArea);


}
function createNewCard() {
    let newForm = document.getElementById('newCardForm');

    let name;
    let props = {};
    let nextProp;
    for (let i = 0; i < newForm.children.length; i++) {
        let node = newForm.children[i];
        if (node.tagName === "LABEL") {
            nextProp = node.children[0].value;
            console.log(nextProp);
        }
        if (node.tagName === "INPUT") {
            if (nextProp != "") {
                console.log(node.value);
                props[nextProp] = parseInt(node.value);
            }
        }
    }
    console.log(props)
    name = document.getElementById('titleEdit').value;
    all_cards.push(new Card(name, props));
    document.getElementById('editwrapper').style.display = 'inline';
    document.getElementById('newCardForm').style.display = 'none';
    console.log(all_cards);
    designateIDs();
    getAllCardProps();
    draw(all_cards[all_cards.length-1].create_element(active=true), 'editAll')
}
function designateIDs() {
    for (let i = 0; i < all_cards.length; i++) {
        all_cards[i].id = i+1;
    }
}
// make cards
all_cards = [
    new Card('Thing 1', {
        'value1': 300,
        'value2': 200,
        'value3': 260
    }),
    new Card('Thing 2', {
        'value1': 243,
        'value2': 654,
        'value3': 234
    }),
];


// add-ids

designateIDs();
getAllCardProps();

var all_players = [new Player('Player 1'), new Player('Player 2')];
var current_player;

// draw_all();
for (let i = 0; i < all_cards.length; i++) {
    draw(all_cards[i].create_element(active=true), 'editAll')
}
//setDropdownProps(all_players[0].cards[0], all_players[1].cards[0]);
