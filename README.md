# Top Trumps
## What is This
This is a game of top trumps made with javascript.

### DISCLAIMER
This project was made in a short space of time for a school deadline. The code is not the best quality it could be. If you have any ideas for improvements please feel free to start an issue.

## How to use
Two example cards are given. The user is expected to edit these cards and add more cards. When tying a propetry name already exisitng property names will appear benith (to stop issues with you having a prop called 'speed' and a prop called 'Speed')

When the user clicks start_game, the cards are randomly distributed between the players.

Player1 selects the prop they want to compare. they can only see their current card and none of the oppenents cards (as these are face down).

Player1 clicks compare and Player2's top card is releaved and the winner is rewarded both cards. If it is a draw both cards exit from the game.

It is now player2's go and they do what player1 did.

## Accreditation
A javascript array shuffeling function was taken from https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array.
